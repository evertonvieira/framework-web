# Framework Web

Estrutura desenvolvida com base no CakePHP (versão 2.7.9), para desenvolvimento de site.

Visual do sistema administrativo baseado no tema [SB Admin 2](https://startbootstrap.com/template-overviews/sb-admin/).

Módulos básicos:
 - Sistema de login básico;
 - Gerenciamento de páginas;
 - Gerenciameto de contatos (formulári de contato);

Módulos Intermediários:
 - Gerenciamento de Posts (notícias);
 - Gerenciamento de Categorias/Posts;
